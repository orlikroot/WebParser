package ua.dp.krotov.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by E.Krotov on 19.04.2016. (e.krotov@hotmail.com))
 */

@Entity
@Table(name = "CONTACT")
public class Contact implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "URL")
    private String url;

    @Column(name = "NAME")
    private String name;

    @Column(name = "Info", length = 1000)
    private String info;

    public Contact(){}

    public Contact(String url, String name, String info) {
        this.url = url;
        this.name = name;
        this.info = info;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String contact) {
        this.info = contact;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return getUrl().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        Contact contact = (Contact) obj;
        return this.getUrl().hashCode()== contact.getUrl().hashCode();
    }
}
