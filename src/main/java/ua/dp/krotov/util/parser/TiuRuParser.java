package ua.dp.krotov.util.parser;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;
import ua.dp.krotov.model.Contact;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by E.Krotov on 20.04.2016. (e.krotov@hotmail.com))
 */
@Service("tiuRuParser")
public class TiuRuParser{

    private static final Logger logger = Logger.getLogger(TiuRuParser.class);

    private int pageCount;
    private String query;
    private Contact contact;
    private Set<Contact> contacts;


    public Set<Contact> makeMagic(String query) {
        this.query = query;
        logger.info(">>>>>>>>>>>>>>>>>>>>Request query = " + query);
        contacts = new HashSet<Contact>();
//        try {
//            Document doc = Jsoup.connect(query).get();
//            String page = doc.select("body > div:nth-child(9) > div.b-grids > div:nth-child(3) > div.b-catalog-panel.h-mt-25.qa-page-pagination > div.b-pager.qa-page-pagination.h-mt-7.b-pager_align_left > div:nth-child(3) > a.b-pager__link.js-page").text();
//            logger.info(">>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<< " + page);
//            pageCount = Integer.parseInt(page);
//            logger.info("Count page = " + pageCount);
//            Elements links = doc.select("a[href$=/]");
//            for (Element link : links) {
//                contact = new Contact();
//                contact.setUrl(link.attr("abs:href"));
//                contact.setName(trim(link.text(), 35));
//                logger.info("Found new URL on page 1 , URL => " + contact.getUrl());
//                contacts.add(contact);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        Document doc = null;
        try {
            doc = Jsoup.connect(query).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String page = doc.select("body > div:nth-child(9) > div.b-grids > div:nth-child(3) > div.b-catalog-panel.h-mt-25.qa-page-pagination > div.b-pager.qa-page-pagination.h-mt-7.b-pager_align_left > div:nth-child(3) > a.b-pager__link.js-page").text();
        logger.info(">>>>>>>>>>>>>>>>>>>> PAGE COUNT <<<<<<<<<<<<<<<<<<<< " + page);
        try {
            pageCount = Integer.parseInt(page);
        } catch (Exception e) {pageCount = 1;}


        for (int i = 1; i <= pageCount; i++) {

            try {
                logger.info(">>>Start parsing page = " + i);
                String urlNext = query + "&page=" + i;
                Document docNext = Jsoup.connect(urlNext).get();
                Elements linksNext = docNext.select("a[href$=/]");
                for (Element link : linksNext) {
                    contact = new Contact();
                    contact.setUrl(link.attr("abs:href"));
                    contact.setName(trim(link.text(), 35));
                    logger.info(">>>Found new URL on page = " + i + " , URL => " + contact.getUrl());
                    try{
                        Document docContacts = Jsoup.connect(link.attr("abs:href") + "contacts").get();
                        logger.info(">>>Search contact phone by link - " + (link.attr("abs:href") + "contacts"));
                        Elements info = docContacts.getElementsByAttributeValue("class", "b-contact-info__phone js-ga-phone-hover");
                        logger.info(">>>contact phone - " + info.text());
                        String phone = info.text().replaceAll("[А-Яа-я]", "");
                        contact.setInfo(phone);
                        contacts.add(contact);
                        Thread.sleep(3000);
                    } catch (Exception e) {
                contact.setInfo("контакты доступны только на странице компании");
            }


                }
            } catch (Exception e){}

        }
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>   Found URLs = " + contacts.size());

//        for (Iterator iterator = contacts.iterator(); iterator.hasNext(); ) {
//            contact = (Contact) iterator.next();
//            try {
//                Document docContacts = Jsoup.connect(((Contact) iterator.next()).getUrl() + "contacts").get();
//                logger.info(">>>Search contact phone by link - " + ((Contact) iterator.next()).getUrl() + "contacts");
//                Elements info = docContacts.getElementsByAttributeValue("class", "b-contact-info__phone js-ga-phone-hover");
//                logger.info(">>>contact phone - " + info.text());
////                if (info == null) {info = docContacts.getElementsByAttributeValue("itemprop", "telephone");}
//                String phone = info.text().replaceAll("[А-Яа-я]", "");
//                contact.setInfo(phone);
//
//
//            } catch (Exception e) {
//                contact.setInfo("контакты доступны только на странице компании");
//            }
//
//        }

        return contacts;
    }

    private static String trim(String s, int width) {
        if (s.length() > width)
            return s.substring(0, width-1) + ".";
        else
            return s;

    }
}
