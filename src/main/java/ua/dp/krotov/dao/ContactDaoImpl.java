package ua.dp.krotov.dao;

import org.springframework.stereotype.Repository;
import ua.dp.krotov.model.Contact;

/**
 * Created by E.Krotov on 19.04.2016. (e.krotov@hotmail.com))
 */
@Repository("contactDao")
public class ContactDaoImpl extends AbstractDao implements ContactDao {
    public void saveContact(Contact contact) {
        persist(contact);
    }
}
