package ua.dp.krotov.dao;

import ua.dp.krotov.model.Contact;

/**
 * Created by E.Krotov on 19.04.2016. (e.krotov@hotmail.com))
 */
public interface ContactDao {
    void saveContact(Contact contact);
}
