package ua.dp.krotov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.krotov.dao.ContactDao;
import ua.dp.krotov.model.Contact;

/**
 * Created by E.Krotov on 19.04.2016. (e.krotov@hotmail.com))
 */
@Service("contactService")
@Transactional
public class ContactServiceImpl implements ContactService {

    @Autowired
    private ContactDao dao;
    public void saveContact(Contact contact) {
        dao.saveContact(contact);

    }
}
