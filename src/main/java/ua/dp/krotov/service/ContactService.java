package ua.dp.krotov.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.krotov.model.Contact;

/**
 * Created by E.Krotov on 19.04.2016. (e.krotov@hotmail.com))
 */

public interface ContactService {
    void saveContact(Contact contact);
}
