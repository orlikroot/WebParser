import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ua.dp.krotov.configuration.AppConfig;
import ua.dp.krotov.model.Contact;

import org.hibernate.Session;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ua.dp.krotov.service.ContactService;
import ua.dp.krotov.util.parser.TiuRuParser;

import java.util.*;


/**
 * Created by E.Krotov on 18.04.2016. (e.krotov@hotmail.com))
 */
public class Main {
    public static void main(String[] args) throws Exception {

        List<String> queryList;


        AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        ContactService service = (ContactService) context.getBean("contactService");

		/*
		 * Create Employee1
		 */
//        Contact contact = new Contact();
//       contact.setName("name");
//        contact.setUrl("www");
//        contact.setInfo("wwwwwweeeeee");



		/*
		 * Persist both Employees
		 */
//        service.saveContact(contact);


//        {
//            try
//            {
//                URL url = new URL("http://prom.ua/search?search_scope=product&search_in_region=&type=S&skip_autoselect=1&search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8+%D1%81%D0%BD%D0%B3");
//                URLConnection urlConnection = url.openConnection();
//                HttpURLConnection connection = null;
//                if(urlConnection instanceof HttpURLConnection)
//                {
//                    connection = (HttpURLConnection) urlConnection;
//                }
//                else
//                {
//                    System.out.println("Please enter an HTTP URL.");
//                    return;
//                }
//                BufferedReader in = new BufferedReader(
//                        new InputStreamReader(connection.getInputStream()));
//                String urlString = "";
//                String current;
//                while((current = in.readLine()) != null)
//                {
//                    urlString += current;
//                }
//               // System.out.println(urlString);
//                Document html = Jsoup.parse(urlString, "UTF-8");
//
//                System.out.println(html.body().toString());
//            }catch(IOException e)
//            {
//                e.printStackTrace();
//            }
//        }

//                URL myUrl = new URL("http://tiu.ru/search?search_scope=product&search_in_region=&type=S&skip_autoselect=1&search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8+%D0%BA%D0%B0%D0%B7%D0%B0%D1%85%D1%81%D1%82%D0%B0%D0%BD");
//
//        String charset = "UTF-8";
//
//
//        Scanner urlScanner = new Scanner(myUrl.openStream(), charset);
//        StringBuilder sb = new StringBuilder();
//        while (urlScanner.hasNextLine()) {
//            sb.append(urlScanner.nextLine() + "\n");
//        }
//        urlScanner.close();
//
//        Document doc = Jsoup.parse(sb.toString());
//
////        Elements elements = doc.body().getElementsByAttributeValue("itemprop", "url");
//        Elements elements = doc.body().getElementsByAttributeValue("class", "h-mb-5 b-text-hider h-nowrap");
//
//for (Element element : elements) {
//    element.getElementsByAttributeValue("class", "h-cursor-default b-pro-state").remove();
//    element.getElementsByAttributeValue("class", "b-text-hider__right-shadow").remove();
//    Elements links = doc.select("a[href]");
//    System.out.println(links);
//
//
//
////    System.out.println(element.toString());
//
//}
//
////        System.out.println(elements.last().toString());
//
//        int pageCount;
//        Set<Contact> contactsSet = new HashSet<>();
//        ArrayList<String> urls = new ArrayList<>();
//        urls.add("http://tiu.ru/search?search_scope=product&search_in_region=&type=S&skip_autoselect=1&search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8+%D0%B1%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D1%8C");
//        urls.add("http://tiu.ru/search?search_scope=product&search_in_region=&type=S&skip_autoselect=1&search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8+%D0%BA%D0%B0%D0%B7%D0%B0%D1%85%D1%81%D1%82%D0%B0%D0%BD");
//        urls.add("http://tiu.ru/search?search_scope=product&search_in_region=&type=S&skip_autoselect=1&search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8+%D0%A1%D0%9D%D0%93");
//
//        for (String url : urls) {
////            String url = "http://tiu.ru/search?search_scope=product&search_in_region=&type=S&skip_autoselect=1&search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8+%D0%BA%D0%B0%D0%B7%D0%B0%D1%85%D1%81%D1%82%D0%B0%D0%BD";
//
//            print("Fetching %s...", url);
//            Document doc = Jsoup.connect(url).get();
//            String info1 = doc.select("body > div:nth-child(9) > div.b-grids > div:nth-child(3) > div.b-catalog-panel.h-mt-25.qa-page-pagination > div.b-pager.qa-page-pagination.h-mt-7.b-pager_align_left > div:nth-child(3) > a.b-pager__link.js-page").text();
//            System.out.println(info1 + "     !!!!!!!!!!!!!!!!!!!!!!!!!!!");
//            Elements pages = doc.select("a[href*=&page=]");
//            if (info1.equals("")){pageCount = 1;} else {pageCount = Integer.parseInt(info1);}
//
//
//
//
//            Elements links = doc.select("a[href$=/]");
////        Elements media = doc.select("[src]");
////        Elements imports = doc.select("link[href]");
//
////        print("\nMedia: (%d)", media.size());
////        for (Element src : media) {
////            if (src.tagName().equals("img"))
////                print(" * %s: <%s> %sx%s (%s)",
////                        src.tagName(), src.attr("abs:src"), src.attr("width"), src.attr("height"),
////                        trim(src.attr("alt"), 20));
////            else
////                print(" * %s: <%s>", src.tagName(), src.attr("abs:src"));
////        }
////
////        print("\nImports: (%d)", imports.size());
////        for (Element link : imports) {
////            print(" * %s <%s> (%s)", link.tagName(),link.attr("abs:href"), link.attr("rel"));
////        }
//
//            print("\nLinks: (%d)", links.size());
//            for (Element link : links) {
//                contact = new Contact();
//                contact.setUrl(link.attr("abs:href"));
//                contact.setName(trim(link.text(), 35));
//                // service.saveContact(contact);
//                contactsSet.add(contact);
//                print(" * a: <%s>  (%s)", link.attr("abs:href"), trim(link.text(), 35));
//            }
//
//            for (int i = 2; i <= pageCount; i++) {
//
////                String urlNext = "http://tiu.ru/search?search_scope=product&search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8&skip_autoselect=1&page=" + i + "&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8+%D0%BA%D0%B0%D0%B7%D0%B0%D1%85%D1%81%D1%82%D0%B0%D0%BD&type=S&search_in_region=";
//
//                try {
//                    String urlNext = url + "&page=" + i;
//                    Document docNext = Jsoup.connect(urlNext).get();
//                    Elements linksNext = docNext.select("a[href$=/]");
//                    for (Element link : linksNext) {
//                        contact = new Contact();
//                        contact.setUrl(link.attr("abs:href"));
//                        contact.setName(trim(link.text(), 35));
//                        // service.saveContact(contact);
//                        contactsSet.add(contact);
//                        print(" * a: <%s>  (%s)", link.attr("abs:href"), trim(link.text(), 35));
//                    }
//                } catch (Exception e){}
//
//            }
//
//
//            for (Iterator iterator = contactsSet.iterator(); iterator.hasNext(); ) {
//               contact = (Contact) iterator.next();
//                try {
//                    Document docContacts = Jsoup.connect(((Contact) iterator.next()).getUrl() + "contacts").get();
//
//                                       Elements info = docContacts.getElementsByAttributeValue("class", "b-contact-info__phone js-ga-phone-hover");
//
//                    String phone = info.text().replaceAll("[А-Яа-я]", "");
//                    contact.setInfo(phone);
//
//
//                } catch (Exception e) {
//                    contact.setInfo("контакты доступны только на странице компании");
//                }
//
//            }









//        print("\nPages: (%d)", pages.size());
//        for (Element page : pages) {
//
//            print(" * page: <%s>  (%s)", page.attr("abs:href"), trim(page.text(), 35));
//        }

//       Set<Contact> contacts =  new TiuRuParser().makeMagic("http://tiu.ru/search?search_scope=product&search_in_region=&type=S&skip_autoselect=1&search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8+%D0%B1%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D1%8C");

//        Set<Contact> contacts =  new TiuRuParser().makeMagic("http://tiu.ru/search?search_scope=product&search_in_region=&type=S&skip_autoselect=1&search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8+%D0%BA%D0%B0%D0%B7%D0%B0%D1%85%D1%81%D1%82%D0%B0%D0%BD");

//        Set<Contact> contacts =  new TiuRuParser().makeMagic("http://tiu.ru/search?search_scope=product&search_in_region=&type=S&skip_autoselect=1&search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8+%D0%A1%D0%9D%D0%93");
//        contacts.addAll(contacts1);
//        contacts.addAll(contacts2);
queryList = new ArrayList<String>();
        queryList.add("http://tiu.ru/search?category=282405&search_scope=product&skip_autoselect=1&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8&search_in_region=");
        queryList.add("http://tiu.ru/search?category=282405&search_scope=product&search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8&skip_autoselect=1&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8+%D0%A1%D0%9D%D0%93&search_in_region=");
        queryList.add("http://tiu.ru/search?category=282405&search_scope=product&search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8&skip_autoselect=1&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8+%D0%B1%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D1%8C&search_in_region=");
        queryList.add("http://tiu.ru/search?category=282405&search_scope=product&search=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8&skip_autoselect=1&search_term=%D0%B3%D1%80%D1%83%D0%B7%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B7%D0%BA%D0%B8+%D0%BA%D0%B0%D0%B7%D0%B0%D1%85%D1%81%D1%82%D0%B0%D0%BD&search_in_region=");

        for (String query : queryList) {
            Set<Contact> contacts = new TiuRuParser().makeMagic(query);
            for (Iterator iterator = contacts.iterator(); iterator.hasNext(); ) {
                service.saveContact((Contact) iterator.next());
            }
        }
        }


    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

    private static String trim(String s, int width) {
        if (s.length() > width)
            return s.substring(0, width-1) + ".";
        else
            return s;

        }





}


